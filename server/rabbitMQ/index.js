// Import the required packages
const amqp = require("amqplib");
const dotenv = require("dotenv");
const {Observable, Subject} = require("rxjs");

dotenv.config();

const calculatedProject = new Subject();

class RabbitMQMessagingService{
    constructor(queue){
        this.rabbitSettings = {
            protocol: process.env.RABBIT_PROTOCOL,
            hostname: process.env.RABBIT_HOST,
            port: process.env.RABBIT_PORT,            //Broker port
            username: process.env.RABBIT_NAME,
            password: process.env.RABBIT_PASSWORD,
            vhost: process.env.RABBIT_VHOST,
            authMechanism: process.env.RABBIT_AUTHMECHANISM 
        }
        this.queue = queue;
        this.backUpQueue = 'backup' + queue;
        this.channel;
        this.connection;
        //this.connect();
    }

    async connect(){
        try{
            console.log('[RabbitMQ] - Inside connect function');
            // Connect to amqp server with above settings
            this.connection = await amqp.connect("amqp://guest:guest@10.244.1.20:5672/");
            console.log('[RabbitMQ] - Connection set!');
            // Create a channel
            this.channel = await this.connection.createChannel();
            console.log('[RabbitMQ] - Channel created!');
            // Connect to queue, if it doens't exist then it will create it
            await this.channel.assertQueue(this.queue);
            console.log('[RabbitMQ] - Queue created/linked!');
        }catch(err){
            console.log(err);
            throw err;
        }
    }

    async disconnect(){
        await channel.close();
        await connection.close();
    }

    async produce(data){
        try{
            await this.channel.sendToQueue(this.queue, Buffer.from(JSON.stringify(data)));
            console.log(" [x] Sent %s", data);
        }catch(err){
            console.log(err);
            throw err;
        }
    }

    async subscribe(){
        let message = {
            project: {
                id: -1,
                companyName: '',
                name: '',
                number: 0,
                location: '',
                version: '',
                downloads: 0,
                company_id: 0
            },
            vehicleEmissions: []
        };
        try{
            console.log(`this is subscribing to: ${this.queue}`);
            await this.channel.consume(this.queue, function(data){
                 message = JSON.parse(data.content.toString());
                 console.log(`Recieved msg ${message}`);
                 console.log(message);

                 calculatedProject.next(message);
             }, {
                 noAck: true //when the data is consumed, it will remove from the queue.
             });
        }catch(err){
            console.log(err);
            throw err;
        }
    }

    async getBackUp(){
        let list = [];
        try{
            await this.channel.consume(this.backUpQueue, function(data){
                list = JSON.parse(data.content.toString());
                
                console.log(`Recieved msg ${message.name}`);
                console.log(message);
            }, {
                noAck: false
            });
    
        }catch(err){
            console.log(err);
            throw err;
        }
        return list;
    }

    getSubject(){
        return calculatedProject;
    }
}

module.exports = RabbitMQMessagingService;