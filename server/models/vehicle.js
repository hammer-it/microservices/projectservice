'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Vehicle extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Vehicle.hasOne(models.Activity,{foreignKey: 'vehicle_id'});
      Vehicle.hasMany(models.Engine, {foreignKey: 'vehicle_id'});
      Vehicle.hasOne(models.VehicleEmission, {foreignKey: 'vehicle_id'});
    }
  };
  Vehicle.init({
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    vehicleId: DataTypes.STRING,
    vehicledescription: DataTypes.STRING,
    vehicleType: DataTypes.STRING,
    brand: DataTypes.STRING,
    model: DataTypes.STRING,
    modelYear: DataTypes.INTEGER,
    weight: DataTypes.INTEGER,
  }, {
    sequelize,
    modelName: 'Vehicle',
  });
  return Vehicle;
};