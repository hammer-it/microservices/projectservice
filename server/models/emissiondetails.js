'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class EmissionDetails extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      EmissionDetails.belongsTo(models.VehicleEmission, { foreignKey: 'emission_id', targetKey: 'id'});
    }
  };
  EmissionDetails.init({
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    driveEmission: DataTypes.INTEGER,
    turnEmission: DataTypes.INTEGER,
    totalEmission: DataTypes.INTEGER,
    emission_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'EmissionDetails',
  });
  return EmissionDetails;
};