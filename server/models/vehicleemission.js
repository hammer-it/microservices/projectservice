'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class VehicleEmission extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      VehicleEmission.belongsTo(models.WasteDetails, {foreignKey: 'waste_id', targetKey: 'id'});
      VehicleEmission.hasMany(models.EmissionDetails, {foreignKey: 'emission_id'});
      VehicleEmission.belongsTo(models.Vehicle, {foreignKey: 'vehicle_id', targetKey: 'id'});
      VehicleEmission.belongsTo(models.Project, {foreignKey: 'project_id', targetKey: 'id'})
    }
  };
  VehicleEmission.init({
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    project_id: DataTypes.INTEGER,
    waste_id: DataTypes.INTEGER,
    vehicle_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'VehicleEmission',
  });
  return VehicleEmission;
};