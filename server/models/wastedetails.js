'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class WasteDetails extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      WasteDetails.hasOne(models.VehicleEmission, { foreignKey: 'waste_id'});
    }
  };
  WasteDetails.init({
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    driveWaste: DataTypes.INTEGER,
    turnWaste: DataTypes.INTEGER,
    L100KM: DataTypes.INTEGER,
    L1U: DataTypes.INTEGER,
  }, {
    sequelize,
    modelName: 'WasteDetails',
  });
  return WasteDetails;
};