'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Activity extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
        Activity.belongsTo(models.Vehicle, {foreignKey: 'vehicle_id', targetKey: 'id'})
    }
  };
  Activity.init({
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    description: DataTypes.STRING,
    distance: DataTypes.INTEGER,
    tol: DataTypes.INTEGER,
    dtol: DataTypes.INTEGER,
    workload: DataTypes.INTEGER,
    vehicle_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Activity',
  });
  return Activity;
};