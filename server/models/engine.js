'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Engine extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Engine.belongsTo(models.Vehicle, { foreignKey: 'vehicle_id', targetKey: 'id'});
    }
  };
  Engine.init({
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    emissieNorm: DataTypes.STRING,
    emissionClass: DataTypes.STRING,
    driveType: DataTypes.STRING,
    power: DataTypes.INTEGER,
    fuel: DataTypes.STRING,
    hasSoothFilter: DataTypes.BOOLEAN,
    vehicle_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Engine',
  });
  return Engine;
};