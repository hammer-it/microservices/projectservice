const { Project } = require('../models');
const { VehicleEmission } = require('../models');
const { EmissionDetails } = require('../models');
const { WasteDetails } = require('../models');
const { Vehicle } = require('../models');
const { Engine } = require('../models');
// const { Activity } = require('../models');

module.exports = {

    createProject(project, vehicleEmissions, company_id) {
        //onProjectCalculated();
        return new Promise(async (resolve, reject) => {
            let returnMessage = {
                project: project,
                vehicleEmissions: [],
            }
            let index = 0;
            let engines = [];
            let emissionDetails = [];

            // Create the project
            const tempProject = await Project.create({
                companyName: project.companyName,
                name: project.name,
                number: project.number,
                location: project.location,
                version: project.version,
                downloads: 0,
                company_id: company_id
            })

            console.log('inside promise');
            returnMessage.project.id = tempProject.id
            // Create all the vehicle emissions
            for await (let emission of vehicleEmissions) {
                const waste = await WasteDetails.create({
                    driveWaste: emission.driveWaste,
                    turnWaste: emission.turnWaste,
                    L100KM: emission.L100KM,
                    L1U: emission.L1U,
                });
                const vehicle = await Vehicle.create({
                    vehicleId: emission.vehicle.vehicleId,
                    vehicledescription: emission.vehicle.vehicledescription,
                    vehicleType: emission.vehicle.vehicleType,
                    brand: emission.vehicle.brand,
                    model: emission.vehicle.model,
                    modelYear: emission.vehicle.modelYear,
                    weight: emission.vehicle.weight,
                })
                const primary = await Engine.create({
                    emissieNorm: emission.primaryEngine.emissieNorm,
                    emissionClass: emission.primaryEngine.emissionClass,
                    driveType: emission.primaryEngine.driveType,
                    power: emission.primaryEngine.power,
                    fuel: emission.primaryEngine.fuel,
                    hasSoothFilter: emission.primaryEngine.hasSoothFilter,
                    vehicle_id: vehicle.id
                })
                const secundary = await Engine.create({
                    emissieNorm: emission.secundaryEngine.emissieNorm,
                    emissionClass: emission.secundaryEngine.emissionClass,
                    driveType: emission.secundaryEngine.driveType,
                    power: emission.secundaryEngine.power,
                    fuel: emission.secundaryEngine.fuel,
                    hasSoothFilter: emission.secundaryEngine.hasSoothFilter,
                    vehicle_id: vehicle.id
                })
                const vehicleEmission = await VehicleEmission.create({
                    project_id: tempProject.id,
                    waste_id: waste.id,
                    vehicle_id: vehicle.id
                })
                const nox = await EmissionDetails.create({
                    driveEmission: emission.nox.driveEmission,
                    turnEmission: emission.nox.turnEmission,
                    totalEmission: emission.nox.totalEmission,
                    emission_id: vehicleEmission.id
                });
                const co2 = await EmissionDetails.create({
                    driveEmission: emission.co2.driveEmission,
                    turnEmission: emission.co2.turnEmission,
                    totalEmission: emission.co2.totalEmission,
                    emission_id: vehicleEmission.id
                })

                engines.push(primary);
                engines.push(secundary);
                emissionDetails.push(nox);
                emissionDetails.push(co2);

                let em = {
                    waste: waste,
                    vehicle: vehicle,
                    engines: engines,
                    emissions: emissionDetails
                }

                returnMessage.vehicleEmissions.push(em);
            }

            resolve(returnMessage);
        });
    },

    updateProject(obj) {
        console.log('updating..')

        return new Promise(async (reject, resolve) => {
            const project = obj.project;
            const emissions = obj.vehicleEmissions
            try {
                await Project.upsert(project);
                for await (let emission of emissions) {
                    // Waste update
                    const waste = {
                        id: emission.waste.id,
                        driveWaste: 10,
                        turnWaste: 10,
                        L100KM: 5,
                        L1U: 5,
                    }

                    await Vehicle.upsert(emission.vehicle);

                    for (let index = 0; index < emission.engines.length; index++) {
                        console.log(emission.engines[index])
                        await Engine.upsert(emission.engines[index]);
                    }

                    // Emissions
                    //let emissionDetails = await EmissionDetails.findAll({where: {emission}})
                    for (let index = 0; index < emission.emissions.length; index++) {
                        // emissionDetails[index].dataValues = emission.emissionDetails[index]; 
                        await EmissionDetails.upsert(emission.emissions[index]);
                    }
                    await WasteDetails.upsert(waste);
                }
                return 'success';
            } catch (err) {
                console.log(err);
                throw err;
            }
        })
    }


}