const express = require('express');
const routes = require('./routes');
const cors = require('cors');
const db = require('./models')
const RabbitMQMessagingService = require('./rabbitMQ');
const { updateProject } = require('./logic/projectLogic')

const app = express();
const corsOptions = {
    origin: "*",
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
    allowedHeaders: [
        'Content-Type', 
        'Authorization', 
        'Origin', 
        'x-access-token', 
        'XSRF-TOKEN'
    ], 
    preflightContinue: false 
};

app.use(express.json());
app.use(cors(corsOptions));
app.use('/api/projects', routes);

// Recieves the "freshly" caclulated projects
const EmissionCalculated = new RabbitMQMessagingService('EmissionCalculated'); 

db.sequelize.sync().then((req) => {
    app.listen(process.env.PORT || '3000', () => {
        console.log(`listening on portx: ${process.env.PORT || '3000'}`)
       
    });
    EmissionCalculated.connect().then(() => {
        console.log('subbing to queue...')
        EmissionCalculated.subscribe();
        EmissionCalculated.getSubject().subscribe(res => {
            console.log('inside subscribtion...')
            if(res != null || res != undefined){
                updateProject(res);
            }else{
                console.log(res);
            }
        });
    });
}).catch(err => {
    console.error(err)
})
