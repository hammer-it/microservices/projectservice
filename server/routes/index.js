const express = require('express');
const router = express.Router();
const { Project } = require('../models');
const { VehicleEmission } = require('../models');
const { EmissionDetails } = require('../models');
const { WasteDetails } = require('../models');
const { Vehicle } = require('../models');
const { Engine } = require('../models');
const { Activity } = require('../models');
const project = require('../models/project');
const RabbitMQMessagingService = require('../rabbitMQ');
const { createProject } = require('../logic/projectLogic')

// Sends new projects to the queue
const CalculateEmission = new RabbitMQMessagingService('CalculateEmission');

CalculateEmission.connect();

// Get a list of projects
router.get('/:companyId', async (req, res, next) => {
    const companyId = req.params.companyId;
    Project.findAll({ where: { company_id: companyId } }).then((projects) => {
        res.send(projects);
    }).catch(err => {
        console.log(err);
        //res.sendStatus(err)
        try {
            let temp = projectService.backUpQueue();
            console.log('got back-up');
            console.log(temp);
        }catch(err){
            console.log(err);
        }
    })
});

// Get a specific project
router.get('/:companyId/:projectId', async (req, res, next) => {
    const companyId = req.params.companyId;
    const projectId = req.params.projectId;
    let emissions = [];
    Project.findAll({ where: { company_id: companyId } }).then(projects => {

        projects.forEach(async project => {
            console.log('*');
            if (project.id == projectId) {
                console.log('**');
                const vehicleEmissions = await VehicleEmission.findAll({ where: { project_id: project.id } })
                // forof ipv foreach because forof is synchronosly
                for (let vehcileEmission of vehicleEmissions) {
                    console.log('***');
                    const waste = await WasteDetails.findAll({ where: { id: vehcileEmission.waste_id } });
                    const emissionDetails = await EmissionDetails.findAll({ where: { emission_id: vehcileEmission.id } });
                    const vehicle = await Vehicle.findAll({ where: { id: vehcileEmission.vehicle_id } });
                    const activity = await Activity.findAll({ where: { vehicle_id: vehcileEmission.vehicle_id } });

                    const engines = await Engine.findAll({ where: { vehicle_id: vehcileEmission.id } });

                    const emission = {
                        WasteDatils: waste,
                        NoxEmission: emissionDetails[0],
                        Co2Emission: emissionDetails[1],
                        Vehicle: vehicle[0],
                        Activity: activity
                    }

                    console.log('****');

                    emissions.push(emission);
                }

                console.log('!');
                const msg = {
                    project: project,
                    vehicleEmissions: emissions
                }

                res.send(msg);
            }
        })
    }).catch((err) => {
        console.log(err);
        res.sendStatus(500)
    })
});

// Create a new project
router.post('/', async (req, res, next) => {
    const obj = req.body;
    const project = obj.project;
    const vehicleEmissions = obj.vehicleEmissions;
    const company_id = obj.companyId;

    await createProject(project, vehicleEmissions, company_id).then(msg => {
        console.log('this is return of create project');
        console.log(msg);
        CalculateEmission.produce(msg);
        res.send('Done');
    });

});


// Update a specific vehicle
router.put('/:id');

// function createProject(project, vehicleEmissions, company_id){
//     //onProjectCalculated();
//     return new Promise( async (resolve, reject) => {
//         let returnMessage = {
//             project: project,
//             vehicleEmissions: [],
//         }
//         let index = 0;
//         let engines = [];
//         let emissionDetails = [];

//         // Create the project
//        const tempProject = await Project.create({
//             companyName: project.companyName,
//             name: project.name,
//             number: project.number,
//             location: project.location,
//             version: project.version,
//             downloads: 0,
//             company_id: company_id
//         })

//         console.log('inside promise');
//         returnMessage.project.id = tempProject.id
//         // Create all the vehicle emissions
//         for await(let emission of  vehicleEmissions){
//             const waste = await WasteDetails.create({
//                 driveWaste: emission.driveWaste,
//                 turnWaste: emission.turnWaste,
//                 L100KM: emission.L100KM,
//                 L1U: emission.L1U,
//             }); 
//             const vehicle = await Vehicle.create({
//                 vehicleId: emission.vehicle.vehicleId,
//                 vehicledescription:  emission.vehicle.vehicledescription,
//                 vehicleType:  emission.vehicle.vehicleType,
//                 brand:  emission.vehicle.brand,
//                 model:  emission.vehicle.model,
//                 modelYear:  emission.vehicle.modelYear,
//                 weight:  emission.vehicle.weight,
//             })
//             const primary =await Engine.create({
//                 emissieNorm: emission.primaryEngine.emissieNorm,
//                 emissionClass:  emission.primaryEngine.emissionClass,
//                 driveType:  emission.primaryEngine.driveType,
//                 power:  emission.primaryEngine.power,
//                 fuel:  emission.primaryEngine.fuel,
//                 hasSoothFilter: emission.primaryEngine.hasSoothFilter,
//                 vehicle_id: vehicle.id
//             })
//             const secundary = await Engine.create({
//                 emissieNorm: emission.secundaryEngine.emissieNorm,
//                 emissionClass:  emission.secundaryEngine.emissionClass,
//                 driveType:  emission.secundaryEngine.driveType,
//                 power:  emission.secundaryEngine.power,
//                 fuel:  emission.secundaryEngine.fuel,
//                 hasSoothFilter:  emission.secundaryEngine.hasSoothFilter,
//                 vehicle_id: vehicle.id
//             })
//             const vehicleEmission = await VehicleEmission.create({
//                     project_id: tempProject.id,
//                     waste_id: waste.id,
//                     vehicle_id: vehicle.id
//             })
//             const nox = await  EmissionDetails.create({
//                 driveEmission: emission.nox.driveEmission,
//                 turnEmission: emission.nox.turnEmission,
//                 totalEmission: emission.nox.totalEmission,
//                 emission_id:  vehicleEmission.id
//             });
//             const co2 = await EmissionDetails.create({
//                 driveEmission: emission.co2.driveEmission,
//                 turnEmission: emission.co2.turnEmission,
//                 totalEmission: emission.co2.totalEmission,
//                 emission_id:  vehicleEmission.id
//             })

//             engines.push(primary);
//             engines.push(secundary);
//             emissionDetails.push(nox);
//             emissionDetails.push(co2);
//             console.log('hi ;)')
// ;           console.log(emissionDetails);
//             let em = {
//                 waste: waste,
//                 vehicle: vehicle,
//                 engines: engines,
//                 emissions: emissionDetails
//             }

//             returnMessage.vehicleEmissions.push(em);
//         }

//         resolve(returnMessage);
//     });
// }

// function onProjectCalculated(){
//     EmissionCalculated.subscribe().then(res => {
//         if(res.project.id != -1){
//             console.log(res);
//             updateProject(res);
//         }
//     });
// }

// function updateProject(obj){
//     console.log('updating..')
//     return new Promise(async (reject, resolve) =>{
//         const project = obj.project;
//         const emissions = obj.vehicleEmissions

//         await Project.upsert(project);
//         for await(let emission of emissions){
//             // Waste update
//             let waste = WasteDetails.findAll({where: {id: emission.waste.id}});
//             waste = emission.waste;
//             WasteDetails.upsert(waste);

//             // Vehicle update
//             let vehicle = Vehicle.findAll({where: {id: emission.vehicle.id}});
//             vehicle = emission.vehicle;
//             Vehicle.upsert(vehicle);

//             // Engines update
//             let engines = Engine.findAll({where: {vehicle_id: emission.vehicle.id}});
//             for(let index = 0; index < engines.length; index++){
//                 engines[index] = emission.engines[index]; 
//                 Engine.upsert(engines[index]);
//             }
//             let emissionDetails = EmissionDetails.findAll({where: {emission}})
//             for(let index = 0; index < emissionDetails.length; index++){
//                 emissionDetails[index] = emission.emissionDetails[index]; 
//                 EmissionDetails.upsert(emissionDetails[index]);
//             }
//         }

//         resolve();
//     })


// }

module.exports = router;